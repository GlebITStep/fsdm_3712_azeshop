﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AZEShop.Models
{
    public class Product
    {
        public int Id { get; set; }

        [Display(Name = "Product name:")]
        [Required(ErrorMessage = "Where is name?")]
        [MaxLength(100)]
        public string Name { get; set; }

        [Display(Name = "Product price:")]
        [Range(0, 100000)]
        public decimal Price { get; set; }

        [Display(Name = "Product description:")]
        [MaxLength(1000)]
        public string Description { get; set; }

        public string ImageUrl { get; set; }

        public int CategoryId { get; set; }
        public Category Category { get; set; }

        public List<OrderProduct> OrderProducts { get; set; }
    }
}
