﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AZEShop.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace AZEShop.Controllers
{
    public class OrderController : Controller
    {
        private readonly AZEShopDbContext context;

        public OrderController(AZEShopDbContext context)
        {
            this.context = context;
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(Order order)
        {
            order = context.Orders.Add(order).Entity;
            context.SaveChanges();

            List<Product> products = JsonConvert.DeserializeObject<List<Product>>(HttpContext.Session.GetString("cart"));
            foreach (var item in products)
            {
                context.OrderProduct.Add(new OrderProduct { OrderId = order.Id, ProductId = item.Id });
            }
            context.SaveChanges();

            HttpContext.Session.Remove("cart");

            return RedirectToAction("Index", "Home");
        }
    }
}