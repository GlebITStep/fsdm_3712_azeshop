﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AZEShop.Helper;
using AZEShop.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AZEShop.Controllers
{
    public class ProductsController : Controller
    {
        private readonly AZEShopDbContext context;

        public ProductsController(AZEShopDbContext context)
        {
            this.context = context;
        }

        //GET: Products/Create
        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        //POST: Products/Create
        [HttpPost]
        public IActionResult Create(Product product, IFormFile image)
        {
            if (ModelState.IsValid)
            {
                product.CategoryId = 1;

                if (image != null)
                {
                    product.ImageUrl = FileSaver.Save(image);
                }

                context.Add(product);
                context.SaveChanges();
                TempData["Created"] = "Product was created!";
            }
            else
            {
                TempData["Error"] = "Error! Product was not created!";
            }
            return View();
        }
    }
}