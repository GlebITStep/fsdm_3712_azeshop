﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AZEShop.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace AZEShop.Controllers
{
    public class CartController : Controller
    {
        private readonly AZEShopDbContext context;

        public CartController(AZEShopDbContext context)
        {
            this.context = context;
        }

        public IActionResult Add(int id)
        {
            List<Product> cart = null;
            if (HttpContext.Session.Keys.Contains("cart"))
            {
                try
                {
                    var cartJson = HttpContext.Session.GetString("cart");
                    cart = JsonConvert.DeserializeObject<List<Product>>(cartJson);
                }
                catch (Exception)
                {
                    cart = new List<Product>();
                }
            }
            else
            {
                cart = new List<Product>();
            }

            var product = context
                .Products
                .FirstOrDefault(x => x.Id == id);

            cart.Add(product);

            var json = JsonConvert.SerializeObject(cart);
            HttpContext.Session.SetString("cart", json);

            return RedirectToAction("Index", "Home");
        }

        public IActionResult Remove(int id)
        {
            if (HttpContext.Session.Keys.Contains("cart"))
            {
                var cartJson = HttpContext.Session.GetString("cart");
                var cart = JsonConvert.DeserializeObject<List<Product>>(cartJson);
                cart.RemoveAll(x => x.Id == id);
                var json = JsonConvert.SerializeObject(cart);
                HttpContext.Session.SetString("cart", json);
            }

            return RedirectToAction("Index", "Home");
        }
    }
}