﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AZEShop.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace AZEShop.Controllers
{
    public class HomeController : Controller
    {
        private readonly AZEShopDbContext context;

        public HomeController(AZEShopDbContext context)
        {
            this.context = context;
        }

        public IActionResult Index()
        {
            if (HttpContext.Session.Keys.Contains("cart"))
            {
                ViewBag.Cart = JsonConvert.DeserializeObject<List<Product>>(HttpContext.Session.GetString("cart"));
            }
            return View(context.Products);
        }

        public IActionResult Product(int id)
        {
            return View(context
                .Products
                .FirstOrDefault(x => x.Id == id));
        }

        public IActionResult Contacts()
        {
            return View();
        }

        public IActionResult Error()
        {
            return View();
        }
    }
}